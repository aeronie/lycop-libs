autotools_flags_linux_32=( --host=i686-linux-gnu )
autotools_flags_linux_64=( --host=x86_64-linux-gnu )
autotools_flags_windows_32=( --host=i686-w64-mingw32 )
cmake_flags_linux_32=( -DCMAKE_TOOLCHAIN_FILE="$root/config/linux-32.cmake" )
cmake_flags_linux_64=( -DCMAKE_TOOLCHAIN_FILE="$root/config/linux-64.cmake" )
cmake_flags_windows_32=( -DCMAKE_TOOLCHAIN_FILE="$root/config/windows-32.cmake" )
qt_flags_linux_32=( -xplatform "$root/config/linux-g++-32" )
qt_flags_linux_64=( -xplatform "$root/config/linux-g++-64" )
qt_flags_windows_32=( -xplatform "$root/config/win32-g++" )
export PATH="$root/config/bin:$PATH"
export QT_HASH_SEED=0
