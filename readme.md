This library aggregates several other libraries in order to simplify the deployment of [Captain Lycop: Invasion of the Heters](http://aeronie.fr/lycop/index_en.html).

This library is free software: you can redistribute it and/or modify it under the terms of the [GNU Lesser General Public License](http://www.gnu.org/licenses/lgpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the [GNU Lesser General Public License](http://www.gnu.org/licenses/lgpl.html) for more details.

# Building on Linux #

* To build all libraries, use `filter_pkg=* filter_target=* ./build.sh`
* To build only one component, use `filter_pkg=sdl` or `filter_pkg=ogg`
* To build only for 32 bits Windows, use `filter_target=windows-32`
* The generated files are in `./{pkg}/{target}/`

# Building on Windows #

The build script was designed for Linux and GCC, and no effort was made to make it portable.
