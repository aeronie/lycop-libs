#!/bin/bash

set -e
root=$(readlink -f "$0")
root=$(dirname "$root")
export root
. "$root/config/config.sh"
: ${filter_pkg:?}
: ${filter_target:?}

targets=(
	linux-32
	linux-64
	linux-64-dbg
	windows-32
	)
target_linux_32=(linux linux_32 release)
target_linux_64=(linux linux_64 release)
target_linux_64_dbg=(linux linux_64 debug)
target_windows_32=(windows windows_32 release)

_pkg()
{
	eval 'case "$1" in '"$filter_pkg"') ;; *) exit;; esac'
	pkg="$1"
	dir="$root/$pkg"
	mkdir -p "$dir"
	cd "$dir"
}
_target()
{
	eval 'case "$1" in '"$filter_target"') ;; *) exit;; esac'
	target="$1"
	prefix="$root/$pkg/$target"
	tmp="$prefix/tmp"
	mkdir -p "$tmp"
	cd "$tmp"
}
_git()
{
	src="$dir/src"
	git clone "$1" "$src" ||:
	cd "$src"
	git clean -dfx
	git checkout -- .
	git submodule foreach --recursive 'git clean -dfx'
	git submodule foreach --recursive 'git checkout -- .'
	git fetch
	git checkout "$2"
}
_hg()
{
	src="$dir/src"
	hg clone "$1" "$src" ||:
	cd "$src"
	hg revert -a --no-backup
	hg pull
	hg update "$2"
}
_src()
{
	src="$dir/$3"
	url="$1$2"
	file="$2"
	shift 3
	wget -c "$url" -O "$file" "$@"
	rm -fr "$src"
	case "$file" in
	*.tar.*) tar -xaf "$file";;
	*) 7z x "$file";;
	esac
	cd "$src"
}
_build()
{
	for x in "${targets[@]}"
	do
	(
		_target "$x"
		"_build_$1" >build.log 2>&1
	)
	done
}
_using()
{
	for x
	do
	x="${root:?}/$x/${target:?}"
	export PATH="$x/bin:$PATH"
	export CPATH=$(echo "$x"{,/usr}/include{,/i386-linux-gnu,/x86_64-linux-gnu}: "$CPATH" | sed 's,: ,:,g')
	export LIBRARY_PATH=$(echo "$x"{,/usr}/lib{,/i386-linux-gnu,/x86_64-linux-gnu}: "$LIBRARY_PATH" | sed 's,: ,:,g')
	export LD_LIBRARY_PATH="$LIBRARY_PATH"
	export PKG_CONFIG_LIBDIR=$(echo "$x"{,/usr}{/lib{,/i386-linux-gnu,/x86_64-linux-gnu},/share}/pkgconfig: "$PKG_CONFIG_LIBDIR" | sed 's,: ,:,g')
	export PKG_CONFIG_SYSROOT_DIR=/
	done
}
_with()
{
	flags=()
	for var
	do
	z="$var[@]"
	flags=("${flags[@]}" "${!z}")
	z="target_${target//-/_}[@]"
	for x in "${!z}"
	do
	z="${var}_${x}[@]"
	flags=("${flags[@]}" "${!z}")
	done
	done
}

################################

_build_autotools()
(
	_using "${using[@]}"
	_with autotools_flags "$pkg"_flags "${with[@]}"
	"$src/configure" --prefix "$prefix" "${flags[@]}"
	make -j4 V=1
	make install
)

autotools_flags=(
	--disable-shared
	)

################################

_build_cmake()
(
	_using "${using[@]}"
	_with cmake_flags "$pkg"_flags "${with[@]}"
	cmake -DCMAKE_INSTALL_PREFIX="$prefix" "${flags[@]}" "$src"
	make -j4 install VERBOSE=1
)

cmake_flags_release=(
	-DCMAKE_BUILD_TYPE=Release
	)
cmake_flags_debug=(
	-DCMAKE_BUILD_TYPE=Debug
	)

################################

(
	_pkg base

	_build()
	(
		_target "$1"
		/usr/sbin/multistrap -a "$2" -d "$prefix" -f "$dir/multistrap.ini"
		find -L "$prefix" -type l -printf ' %H%l %p ' | xargs -r -n2 ln -fs
	)

	_build linux-32 i386
	_build linux-64 amd64
	ln -fsT linux-64 linux-64-dbg
)

################################

(
	_pkg gl
	_src http://developer.amd.com/tools-and-sdks/graphics-development/amd-opengl-es-sdk/# GLES_SDK_v31.zip GLES_SDK_v31 --post-data='amd_developer_central_nonce=21792b6c6a&f=R0xFU19TREtfdjMxLnppcA=='

	(
		_target windows-32
		ln -fsT "$src/include" "$prefix/include"
		ln -fsT "$src/x86" "$prefix/lib"
	)
)

################################

(
	_pkg qt
	_git git://code.qt.io/qt/qt5.git 5.4
	"$src/init-repository" --force --module-subset=qtbase,qttools,qtdeclarative,qtquickcontrols,qtxmlpatterns
	git -C qtbase apply "$dir/qtbase.diff"
	git -C qtquickcontrols apply "$dir/qtquickcontrols.diff"
	sql=("$src"/qtbase/src/sql/drivers/*/)
	sql=("${sql[@]%/*}")

	_build_pkg()
	(
		_using base gl
		_with qt_flags
		"$src/configure" -prefix "$prefix" -extprefix "$prefix" "${flags[@]}"
		make -j4
		make install
	)

	qt_flags=(
		-opensource
		-confirm-license
		-static
		-no-reduce-exports
		-nomake examples
		-nomake tests
		-opengl
		"${sql[@]/*\//-no-sql-}"
		-no-glib
		-no-gtkstyle
		-no-directfb
		-no-egl
		-no-eglfs
		-no-kms
		-no-linuxfb
		-no-gif
		-no-alsa
		-no-pulseaudio
		-no-fontconfig
		-no-iconv
		-no-icu
		-no-cups
		-no-dbus
		-no-libudev
		-no-mtdev
		-no-nis
		-no-openssl
		-no-largefile
		-qt-zlib
		-qt-libjpeg
		-qt-libpng
		-qt-harfbuzz
		-qt-pcre
		-v
		)
	qt_flags_release=(
		-release
		-no-qml-debug
		-DNDEBUG
		-D__FILE__="'\\\"\\\"'"
		)
	qt_flags_debug=(
		-debug
		)
	qt_flags_linux=(
		-no-sm
		-qt-freetype
		-qt-xcb
		-qt-xkbcommon
		)
	qt_flags_windows=(
		-no-freetype
		)

	_build pkg
)

################################

(
	_pkg box2d
	_git https://github.com/erincatto/Box2D.git v2.3.1
	git apply "$dir/box2d.diff"
	src="$src/Box2D"

	box2d_flags=(
		-DBOX2D_INSTALL=ON
		-DBOX2D_BUILD_EXAMPLES=OFF
		)

	_build cmake
)

################################

(
	_pkg sdl
	_hg http://hg.libsdl.org/SDL release-2.0.4
	hg import --no-commit "$dir/sdl.diff"

	sdl_flags=(
		--disable-video-dummy
		--disable-render
		--disable-power
		--disable-threads
		--disable-timers
		--disable-loadso
		--disable-cpuinfo
		--disable-atomic
		--disable-dbus
		--disable-libudev
		--disable-sndio
		--disable-diskaudio
		--disable-dummyaudio
		--disable-oss
		--disable-alsa
		--disable-esd
		--disable-pulseaudio
		)
	sdl_flags_linux=(
		--disable-video
		--disable-haptic
	)

	_build autotools
)

################################

(
	_pkg ogg
	_src http://downloads.xiph.org/releases/ogg/ libogg-1.3.2.tar.xz libogg-1.3.2
	_build autotools
)

################################

(
	_pkg vorbis
	_src http://downloads.xiph.org/releases/vorbis/ libvorbis-1.3.5.tar.xz libvorbis-1.3.5
	using=(ogg)

	vorbis_flags=(
		--disable-docs
		--disable-examples
		--disable-oggtest
		)

	_build autotools
)

################################

(
	_pkg theora
	_src http://downloads.xiph.org/releases/theora/ libtheora-1.1.1.tar.xz libtheora-1.1.1
	using=(ogg)

	theora_flags=(
		--disable-oggtest
		--disable-vorbistest
		--disable-sdltest
		--disable-encode
		--disable-examples
		--disable-telemetry
		--disable-spec
		)

	_build autotools
)

################################

(
	_pkg openal
	_git git://repo.or.cz/openal-soft.git openal-soft-1.17.2
	using=(base)

	openal_flags=(
		-DALSOFT_EXAMPLES=OFF
		-DALSOFT_TESTS=OFF
		-DALSOFT_UTILS=OFF
		-DALSOFT_BACKEND_SNDIO=OFF
		-DALSOFT_BACKEND_WAVE=OFF
		-DLIBTYPE=STATIC
		)

	_build cmake
)

################################

(
	_pkg steamworks
	_src http://example.com/ steamworks_sdk_139.zip sdk

	_build()
	(
		_target "$1"
		ln -fsT "$src/public" "$prefix/include"
		ln -fsT "$src/redistributable_bin/$2" "$prefix/lib"
	)

	_build linux-32 linux32
	_build linux-64 linux64
	_build linux-64-dbg linux64
	_build windows-32 ''
	ln -fs "$dir/steam_hack.dll" "$src/redistributable_bin"
)

################################

(
	_pkg lycop

	_build()
	(
		_target "$1"
		_using base qt
		mkdir -p "$prefix/lib"
		(
			cd "$root/qt/$target/qml"
			{
				echo '<RCC><qresource>'
				find QtQml QtQuick/Controls QtQuick/Layouts QtQuick/Window.2 QtQuick.2 -type f '!' -name '*.a' '!' -name '*.prl' -printf '<file>%p</file>'
				echo '</qresource></RCC>'
			} | rcc --name lycop_qml -
		) >qml.cc
		shift
		"$@" "$dir/main.cc" qml.cc $(pkg-config --cflags Qt5Core) -shared -o "$prefix/lib"
	)
	linux()
	{
		"$@/liblycop.so" -Wl,--whole-archive "$root/"{"box2d/$target/lib/libBox2D","sdl/$target/lib/libSDL2","qt/$target/"{lib/libQt5{Concurrent,Quick,Qml,Gui,Core},plugins/platforms/libqxcb,qml/{QtQml/Models.2/libmodelsplugin,QtQml/StateMachine/libqtqmlstatemachine,QtQuick/Controls/libqtquickcontrolsplugin,QtQuick/Layouts/libqquicklayoutsplugin,QtQuick/Window.2/libwindowplugin,QtQuick.2/libqtquick2plugin}},"ogg/$target/lib/libogg","theora/$target/lib/libtheoradec","vorbis/$target/lib/lib"{vorbis,vorbisfile},"openal/$target/lib/libopenal"}.a -Wl,--no-whole-archive "$root/qt/$target/lib/lib"{Qt5{Network,Widgets,PlatformSupport},qtharfbuzzng,xcb-static}.a $(pkg-config --libs gl x11-xcb xi xrender) -ldl -pthread -lrt -static-libstdc++
	}
	windows()
	{
		"$@/lycop.dll" -Wl,-export-all-symbols -Wl,--whole-archive "$root/"{"box2d/$target/lib/libBox2D","sdl/$target/lib/libSDL2","qt/$target/"{lib/libQt5{Concurrent,Quick,Qml,Gui,Core},plugins/platforms/libqwindows,qml/{QtQml/Models.2/libmodelsplugin,QtQml/StateMachine/libqtqmlstatemachine,QtQuick/Controls/libqtquickcontrolsplugin,QtQuick/Layouts/libqquicklayoutsplugin,QtQuick/Window.2/libwindowplugin,QtQuick.2/libqtquick2plugin}},"ogg/$target/lib/libogg","theora/$target/lib/libtheoradec","vorbis/$target/lib/lib"{vorbis,vorbisfile},"openal/$target/lib/libOpenAL32"}.a -Wl,--no-whole-archive "$root/qt/$target/lib/lib"{Qt5{Network,Widgets,PlatformSupport},qtharfbuzzng}.a -lgdi32 -limm32 -lmpr -lole32 -loleaut32 -lopengl32 -luuid -lversion -lwinmm -lws2_32 -static-libgcc -static-libstdc++
	}

	_build linux-32 linux i686-linux-gnu-g++ -s -Os
	_build linux-64 linux x86_64-linux-gnu-g++ -s -Os
	_build linux-64-dbg linux x86_64-linux-gnu-g++ -g -Og
	_build windows-32 windows i686-w64-mingw32-g++ -s -Os
)

################################
